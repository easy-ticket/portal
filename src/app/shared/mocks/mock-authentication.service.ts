import {Injectable} from '@angular/core';
import {Observable, of, Subject, throwError} from 'rxjs';
import {User} from '../models/user';
import {Token} from '../models/token';
import {Client} from '../models/client';

@Injectable({
  providedIn: 'root'
})
export class MockAuthenticationService {

  user: User;
  public userObservable = new Subject<User>();

  constructor() {
    this.setCurrentUser(JSON.parse(localStorage.getItem('currentUser')));
  }

  getCurrentUser() {
    return this.user;
  }

  setCurrentUser(user: User) {
    this.user = user;
    this.notifyUserChange(this.user);
  }

  notifyUserChange(user) {
    this.userObservable.next(user);
  }

  userIsGuest() {
    return this.user === null || this.user === undefined;
  }

  login(username: string, password: string): Observable<Token> {
    if (username === 'testError') {
      return throwError(new Error(''));
    } else {
      return of(new Token('', ''));
    }
  }

  getUser(username: string): Observable<object> {
    const user = new Client('', '' , '' , '','', '', '', '');
    this.setCurrentUser(user);
    return of(user);
  }

  register(client: Client): Observable<object> {
    const mockResponse = {status: true};
    return of(mockResponse);
  }
}
