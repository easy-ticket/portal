export class BuyTicket {
  document_number: string;
  event_seat: number;

  constructor(document?: string, event_seat?: number) {
    this.document_number = document;
    this.event_seat = event_seat;
  }
}
