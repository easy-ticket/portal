import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventCreateComponent} from './event-create.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {Router} from '@angular/router';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {EventsService} from '../../../shared/services/events/events.service';
import {MockEventsService} from '../../../shared/mocks/mock-events.service';
import {LocationsService} from '../../../shared/services/locations/locations.service';
import {MockLocationsService} from '../../../shared/mocks/mock-locations.service';

describe('EventCreateComponent', () => {
  let component: EventCreateComponent;
  let fixture: ComponentFixture<EventCreateComponent>;

  const router = {
    // Mock function
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule
      ],
      declarations: [
        EventCreateComponent
      ],
      providers: [
        { provide: Router, useValue: router},
        { provide: EventsService, useClass: MockEventsService },
        { provide: LocationsService, useClass: MockLocationsService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
