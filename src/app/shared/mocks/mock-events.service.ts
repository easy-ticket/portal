import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {EventType} from '../models/eventType';
import {Event} from '../models/event';

@Injectable({
  providedIn: 'root'
})
export class MockEventsService {

  eventTypes: EventType[] = [
    new EventType(1, 'Concert'),
    new EventType(2, 'Play'),
  ];

  events: Event[] = [
    new Event(1, '01/01/2018 10:30:00', 'Test description', 20, 'Event 1', 1, 1, 1)
  ];

  constructor() { }

  getEventTypes(): Observable<EventType[]> {
    return of(this.eventTypes);
  }


  getEvents(): Observable<Event[]> {
    return of(this.events);
  }

  getEventById(id: number): Observable<Event> {
    let specificEvent: Event;
    for (const event of this.events) {
      if (event.id === id) {
        specificEvent = event;
        break;
      }
    }
    return of(specificEvent);
  }

  getEventTypeById(id: number): Observable<EventType> {
    let specificEventType: EventType;
    for (const eventType of this.eventTypes) {
      if (eventType.id === id) {
        specificEventType = eventType;
        break;
      }
    }
    return of(specificEventType);
  }
}
