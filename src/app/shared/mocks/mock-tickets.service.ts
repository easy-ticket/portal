import {Injectable} from '@angular/core';
import {TicketData} from '../models/ticket-data';
import {of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MockTicketsService {

  tickets: TicketData[] = [
    new TicketData(1, 1, '123456789', 1, 1, 1, 1, '1A', 'Test event')
  ];

  constructor() { }

  getTicketsByUsername(username: string) {
    return of(this.tickets);
  }
}
