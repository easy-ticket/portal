import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Event} from '../../models/event';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../authentication/authentication.service';
import {EventType} from '../../models/eventType';
import {Locality} from '../../models/locality';
import {EventZone} from '../../models/event-zone';

@Injectable({
  providedIn: 'root'
})
export class EventsService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    this.apiUrl = environment.apiUrl;
  }

  getHttpHeaders() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Token: ${this.authenticationService.getCurrentToken()}`
      })
    };
  }

  // EventType operations
  // -------------------------------

  getEventTypes(): Observable<EventType[]> {
    const url = `${this.apiUrl}/event-type/`;
    return this.http.get<EventType[]>(url, this.getHttpHeaders());
  }

  getEventTypeById(id: number): Observable<EventType> {
    const url = `${this.apiUrl}/event-type/${id}/`;
    return this.http.get<EventType>(url, this.getHttpHeaders());
  }

  // Event operations
  // -------------------------------

  createEvent(event: Event, zones?: Locality[]): Observable<object> {
    const url = `${this.apiUrl}/event/`;

    const data = {
      event: event,
      zones: zones
    };

    return this.http.post<object>(url, data, this.getHttpHeaders());
  }

  getEvents(status?: number): Observable<Event[]> {
    if (this.authenticationService.getCurrentRole() === 'organizer') {
      return this.getEventsByOrganizer(this.authenticationService.getCurrentUser().username);
    }
    const params = new HttpParams();
    let url = `${this.apiUrl}/event/`;
    if (status !== undefined) {
      url += `?status=${status}`;
    }
    return this.http.get<Event[]>(url, this.getHttpHeaders());
  }

  getEventsByOrganizer(username: string): Observable<Event[]> {
    const url = `${this.apiUrl}/user/organizer/${username}/event/`;
    return this.http.get<Event[]>(url, this.getHttpHeaders());
  }

  getEventById(id: number): Observable<Event> {
    const url = `${this.apiUrl}/event/${id}/`;
    return this.http.get<Event>(url, this.getHttpHeaders());
  }

  editEvent(event: Event): Observable<object> {
    const url = `${this.apiUrl}/event/${event.id}/`;
    return this.http.put<object>(url, event, this.getHttpHeaders());
  }

  deleteEvent(event: Event): Observable<object> {
    const url = `${this.apiUrl}/event/${event.id}/`;
    return this.http.delete<object>(url, this.getHttpHeaders());
  }

  getEventSeats(id: number): Observable<EventZone[]> {
    const url = `${this.apiUrl}/event/${id}/seats/`;
    return this.http.get<EventZone[]>(url, this.getHttpHeaders());
  }
}
