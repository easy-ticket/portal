import {Injectable} from '@angular/core';
import {Event} from '../models/event';

@Injectable({
  providedIn: 'root'
})
export class MockBuyTicketDataService {

  event: Event;

  constructor() {
    this.event = new Event(1, '01/01/2018 10:30:00', 'Test description', 20, 'Event 1', 1, 1, 1)
  }

  getEvent() {
    return this.event;
  }
}
