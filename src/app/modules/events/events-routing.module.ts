import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {EventsListComponent} from './events-list/events-list.component';
import {EventDetailComponent} from './event-detail/event-detail.component';
import {EventCreateComponent} from './event-create/event-create.component';
import {EventEditComponent} from './event-edit/event-edit.component';

const routes: Routes = [
  {path: 'list', component: EventsListComponent, runGuardsAndResolvers: 'always'},
  {path: 'detail/:id', component: EventDetailComponent},
  {path: 'create', component: EventCreateComponent},
  {path: 'edit/:id', component: EventEditComponent},
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  // imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: 'reload'})],
  exports: [RouterModule]
})
export class EventsRoutingModule { }
