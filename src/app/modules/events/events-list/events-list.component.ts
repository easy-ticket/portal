import {Component, OnDestroy, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {Event} from '../../../shared/models/event';
import {EventsService} from '../../../shared/services/events/events.service';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-events-list',
  templateUrl: './events-list.component.html',
  styleUrls: ['./events-list.component.css']
})
export class EventsListComponent implements OnInit, OnDestroy {

  navigationSubscription;

  events: Event[];
  disabledEvents: Event[];
  eventTypes: {[key: number]: string; } = {};

  images;

  closeResult: string;
  selectedEvent: Event;

  constructor(
    private authenticationService: AuthenticationService,
    private eventsService: EventsService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.disabledEvents = [];
    this.images = [1, 2, 3, 4, 5].map(() => `https://picsum.photos/200/200?random&t=${Math.random()}`);
  }

  ngOnInit() {
    this.getEventTypes();
    this.getEvents();
  }

  ngOnDestroy() {
    if (this.navigationSubscription) {
      this.navigationSubscription.unsubscribe();
    }
  }

  getEvents() {
    this.eventsService.getEvents(1).subscribe(events => {
      this.processEvents(events);
    });
  }

  getEventTypes() {
    this.eventsService.getEventTypes().subscribe(types => {
      for (const type of types) {
        this.eventTypes[type.id] = type.name;
      }
    });
  }

  processEvents(events: Event[]) {
    const activeEvents = [];
    if (this.isOrganizer()) {
      for (const event of events) {
        if (event.status === 3) {
          this.disabledEvents.push(event);
        } else if (event.status === 1) {
          activeEvents.push(event);
        }
      }
    }
    this.events = activeEvents.length > 0 ? activeEvents : events;
  }

  redirectToCreateEvent() {
    this.router.navigate(['/events/create']);
  }

  redirectToEditEvent(id: number) {
    this.router.navigate([`/events/edit/${id}`]);
  }

  openDeleteEventModal(content, event) {
    this.selectedEvent = event;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  deleteEvent(id: number) {
    this.eventsService.getEventById(id).subscribe(event => {
      this.eventsService.deleteEvent(event).subscribe(response => {
        this.getEvents();
      }, error => {
        console.log('error: ' + error);
      });
    });
  }

  isOrganizer() {
    return this.authenticationService.userIsOrganizer();
  }

  isClient() {
    return this.authenticationService.userIsClient();
  }

  isGuest() {
    return this.authenticationService.userIsGuest();
  }
}
