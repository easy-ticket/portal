import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';

import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HeaderComponent} from './shared/components/header/header.component';
import {NavbarComponent} from './shared/components/navbar/navbar.component';
import {AboutComponent} from './shared/components/about/about.component';
import {HttpClientModule} from '@angular/common/http';
import {AngularFontAwesomeModule} from 'angular-font-awesome';

// import {fakeBackendProvider} from './shared/mocks/fakeBackend';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavbarComponent,
    AboutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
    AngularFontAwesomeModule
  ],
  providers: [
    // fakeBackendProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
