export class Locality {

  id: number;
  name: string;
  seats: number;
  price: number;

  constructor(name?: string, seats?: number, price?: number, id?: number) {
    this.name = name;
    this.seats = seats;
    this.price = price;
    if (id !== undefined) {
      this.id = id;
    }
  }
}
