import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {TicketBuyComponent} from './ticket-buy/ticket-buy.component';
import {TicketsListComponent} from './tickets-list/tickets-list.component';

const routes: Routes = [
  {path: 'buy', component: TicketBuyComponent},
  {path: 'list', component: TicketsListComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TicketsRoutingModule { }
