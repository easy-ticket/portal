import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AboutComponent} from './shared/components/about/about.component';

const routes: Routes = [
  { path: 'about', component: AboutComponent },
  { path: '', redirectTo: '/events/list', pathMatch: 'full' },

  {path: 'authentication', loadChildren: './modules/authentication/authentication.module#AuthenticationModule'},
  {path: 'events', loadChildren: './modules/events/events.module#EventsModule'},
  {path: 'tickets', loadChildren: './modules/tickets/tickets.module#TicketsModule'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
