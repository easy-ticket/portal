import {User} from './user';

export class Client extends User {

  birthdate: string;
  document_number: string;
  document_type: string;

  constructor(email: string, username: string, first_name: string, last_name: string, password: string,
              birthdate: string, document_number: string, document_type: string) {
    super(email, username, first_name, last_name, password);
    this.birthdate = birthdate;
    this.document_number = document_number;
    this.document_type = document_type;
  }
}
