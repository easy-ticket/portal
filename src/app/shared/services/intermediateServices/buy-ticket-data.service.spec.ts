import {TestBed} from '@angular/core/testing';

import {BuyTicketDataService} from './buy-ticket-data.service';

describe('BuyTicketDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BuyTicketDataService = TestBed.get(BuyTicketDataService);
    expect(service).toBeTruthy();
  });
});
