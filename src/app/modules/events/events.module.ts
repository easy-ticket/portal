import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EventsListComponent} from './events-list/events-list.component';
import {EventDetailComponent} from './event-detail/event-detail.component';
import {EventCreateComponent} from './event-create/event-create.component';
import {EventEditComponent} from './event-edit/event-edit.component';
import {EventsRoutingModule} from './events-routing.module';
import {EventsService} from '../../shared/services/events/events.service';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {ReadableDatePipe} from '../../shared/pipes/readable-date.pipe';
import {CookieService} from 'ngx-cookie-service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    ReactiveFormsModule,
    EventsRoutingModule
  ],
  declarations: [EventsListComponent, EventDetailComponent, EventCreateComponent, EventEditComponent, ReadableDatePipe],
  providers: [EventsService, CookieService]
})
export class EventsModule { }
