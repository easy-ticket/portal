import {TestBed} from '@angular/core/testing';

import {MockTicketsService} from './mock-tickets.service';

describe('MockTicketsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockTicketsService = TestBed.get(MockTicketsService);
    expect(service).toBeTruthy();
  });
});
