import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventsListComponent} from './events-list.component';
import {HttpClientModule} from '@angular/common/http';
import {Router} from '@angular/router';
import {EventsService} from '../../../shared/services/events/events.service';
import {MockEventsService} from '../../../shared/mocks/mock-events.service';

describe('EventsListComponent', () => {
  let component: EventsListComponent;
  let fixture: ComponentFixture<EventsListComponent>;

  const router = {
    // Mock function
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule
      ],
      declarations: [
        EventsListComponent
      ],
      providers: [
        { provide: Router, useValue: router},
        { provide: EventsService, useClass: MockEventsService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
