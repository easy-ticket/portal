import {Injectable} from '@angular/core';

import {Event} from '../../models/event';
import {EventZone} from '../../models/event-zone';

@Injectable({
  providedIn: 'root'
})
export class BuyTicketDataService {

  event: Event;
  eventZone: EventZone;

  constructor() { }

  setEvent(event: Event) {
    this.event = event;
  }

  getEvent() {
    return this.event;
  }

  setEventZone(eventZone: EventZone) {
    this.eventZone = eventZone;
  }

  getEventZone() {
    return this.eventZone;
  }
}
