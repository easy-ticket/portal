import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventsService} from '../../../shared/services/events/events.service';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {Event} from '../../../shared/models/event';
import {User} from '../../../shared/models/user';
import {EventType} from '../../../shared/models/eventType';
import {EventLocation} from '../../../shared/models/eventLocation';
import {NgbDateStruct, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {LocationsService} from '../../../shared/services/locations/locations.service';

@Component({
  selector: 'app-event-edit',
  templateUrl: './event-edit.component.html',
  styleUrls: ['./event-edit.component.css']
})
export class EventEditComponent implements OnInit {

  user: User;
  event: Event = new Event();

  eventDate: NgbDateStruct;
  eventTime: NgbTimeStruct;
  meridian = true;

  event_types: EventType[];
  locations: EventLocation[];

  constructor(private route: ActivatedRoute,
              private router: Router,
              private eventsService: EventsService,
              private locationsService: LocationsService,
              private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.requestEvent();
    this.requestEventTypes();
    this.requestLocations();
  }

  requestEventTypes() {
    this.eventsService.getEventTypes().subscribe(
      eventTypes => {
        this.event_types = eventTypes;
      }
    );
  }

  requestLocations() {
    this.locationsService.getLocations().subscribe(
      locations => {
        this.locations = locations;
      }
    );
  }

  setDatetime(dateStr: string) {
    const dateList = dateStr.split(' ');
    const date = dateList[0];
    const time = dateList[1];
    const dateArray = date.split('/');
    const timeArray = time.split(':');

    this.eventDate = new class implements NgbDateStruct {
      day: number;
      month: number;
      year: number;
    };

    this.eventDate.day = +dateArray[0];
    this.eventDate.month = +dateArray[1];
    this.eventDate.year = +dateArray[2];

    this.eventTime = new class implements NgbTimeStruct {
      hour: number;
      minute: number;
      second: number;
    };

    this.eventTime.hour = +timeArray[0];
    this.eventTime.minute = +timeArray[1];
    this.eventTime.second = +timeArray[2];
  }

  requestEvent() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.eventsService.getEventById(id).subscribe(event => {
      this.event = event;
      this.setDatetime(this.event.date);
    });
  }

  editEvent() {
    this.event.date = `${this.eventDate.day}/${this.eventDate.month}/${this.eventDate.year}`;
    this.event.date += ` ${this.eventTime.hour}:${this.eventTime.minute}:${this.eventTime.second}`;

    this.eventsService.editEvent(this.event).subscribe(
      response => {
        this.router.navigate(['/events/list']);
    });
  }
}
