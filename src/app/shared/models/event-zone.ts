import {EventSeat} from './event-seat';

export class EventZone {
  name: string;
  price: number;
  event_seat: EventSeat[];
}
