import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {LoginComponent} from './login.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {MockAuthenticationService} from '../../../shared/mocks/mock-authentication.service';
import {Router} from '@angular/router';

describe('LoginComponent', () => {
  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;

  const router = {
    // Mock function
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        FormsModule
      ],
      declarations: [
        LoginComponent
      ],
      providers: [
        { provide: Router, useValue: router},
        { provide: AuthenticationService, useClass: MockAuthenticationService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid on empty form', () => {
    fixture.whenStable().then( () => {
      expect(component.loginForm.valid).toBeFalsy();
    });
  });

  it('should check username filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const username = component.loginForm.controls['username'];

      expect(username.valid).toBeFalsy();

      errors = username.errors || {};
      expect(errors['required']).toBeTruthy();

      username.setValue('test');
      errors = username.errors || {};
      expect(errors['required']).toBeFalsy();
    });
  });

  it('should check password filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const password = component.loginForm.controls['password'];

      expect(password.valid).toBeFalsy();

      errors = password.errors || {};
      expect(errors['required']).toBeTruthy();

      password.setValue('pass');
      errors = password.errors || {};
      expect(errors['required']).toBeFalsy();
    });
  });

  it('should display error message when credentials are invalid', () => {
    const compiled = fixture.debugElement.nativeElement;
    fixture.whenStable().then( () => {
      const username = component.loginForm.controls['username'];
      const password = component.loginForm.controls['password'];

      username.setValue('testError');
      password.setValue('pass');

      expect(component.loginForm.valid).toBeTruthy();
      component.login();

      fixture.detectChanges();

      expect(component.error).toBeTruthy();
      expect(component.errorMessage).toBe('Usuario o contraseña incorrectos');
      expect(compiled.querySelector('span').innerHTML).toContain('Usuario o contraseña incorrectos');
    });
  });

  it('should navigate on successful login', () => {
    fixture.whenStable().then( () => {
      const username = component.loginForm.controls['username'];
      const password = component.loginForm.controls['password'];

      username.setValue('testClient');
      password.setValue('pass');

      expect(component.loginForm.valid).toBeTruthy();
      component.login();

      fixture.detectChanges();

      expect(component.error).toBeFalsy();
      expect(component.errorMessage).toBe('');
      expect(router.navigate).toHaveBeenCalledWith(['/events/list']);
    });
  });

});
