import {TestBed} from '@angular/core/testing';

import {MockBuyTicketDataService} from './mock-buy-ticket-data.service';

describe('MockBuyTicketDataService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockBuyTicketDataService = TestBed.get(MockBuyTicketDataService);
    expect(service).toBeTruthy();
  });
});
