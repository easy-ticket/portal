import {User} from './user';

export class Organizer extends User {
  contact_email: string;
  contact_name: string;
  name: string;
  nit: string;
  phone: string;
  user: string;

  constructor(email?: string, username?: string, first_name?: string, last_name?: string, password?: string,
              contact_email?: string, contact_name?: string, name?: string, nit?: string, phone?: string,
              user?: string) {
    super(email, username, first_name, last_name, password);
    this.contact_email = contact_email;
    this.contact_name = contact_name;
    this.name = name;
    this.nit = nit;
    this.phone = phone;
    this.user = user;
  }
}
