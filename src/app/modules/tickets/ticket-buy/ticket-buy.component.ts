import {Component, OnInit, ViewChild} from '@angular/core';
import {BuyTicketDataService} from '../../../shared/services/intermediateServices/buy-ticket-data.service';
import {Event} from '../../../shared/models/event';
import {EventsService} from '../../../shared/services/events/events.service';
import {EventZone} from '../../../shared/models/event-zone';
import {BuyTicket} from '../../../shared/models/buy-ticket';
import {EventSeat} from '../../../shared/models/event-seat';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {Client} from '../../../shared/models/client';
import {Md5} from 'ts-md5/dist/md5';
import {DomSanitizer} from '@angular/platform-browser';
import {TicketsService} from '../../../shared/services/tickets/tickets.service';
import {CookieService} from 'ngx-cookie-service';
import {Router} from '@angular/router';
import {ModalDismissReasons, NgbModal, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-ticket-buy',
  templateUrl: './ticket-buy.component.html',
  styleUrls: ['./ticket-buy.component.css']
})
export class TicketBuyComponent implements OnInit {

  @ViewChild('modalContent') modalContent: any;

  event: Event;
  eventZones: EventZone[];

  totalPrice = 0;
  ticketCount = 0;
  ticketAmountPerZone: number[];
  zoneTickets: BuyTicket[][];
  zoneSeats: EventSeat[][];

  ticketIds;

  cvv;
  cardNumber;
  expirationDate;
  cardName;
  address;

  payUUrl;
  deviceSessionId;

  step: string;

  client: Client;

  modalReference: NgbModalRef;
  closeResult: string;

  transaction = {
    order: {
      accountId: '512321',
      referenceCode: 'TestPayU',
      description: '',
      language: 'es',
      signature: '7ee7cf808ce6a39b17481c54f2c57acc',
      additionalValues: {
        TX_VALUE: {
          value: 10,
          currency: 'COP'
        },
        TX_TAX: {
          value: 0,
          currency: 'COP'
        },
        TX_TAX_RETURN_BASE: {
          value: 0,
          currency: 'COP'
        }
      },
      buyer: {
        fullName: '',
        emailAddress: '',
        contactPhone: '3100000000',
        dniNumber: '',
        shippingAddress: {
          street1: '',
          street2: 'Test',
          city: 'Bogotá',
          state: 'Cundinamarca',
          country: 'CO',
          postalCode: '000000',
          phone: '1234567'
        }
      },
      shippingAddress: {
        street1: '',
        street2: 'Test',
        city: 'Bogotá',
        state: 'Cundinamarca',
        country: 'CO',
        postalCode: '000000',
        phone: '1234567'
      }
    },
    payer: {
      fullName: '',
      emailAddress: '',
      contactPhone: '7563126',
      dniNumber: '',
      billingAddress: {
        street1: '',
        street2: 'Test',
        city: 'Bogotá',
        state: 'Cundinamarca',
        country: 'CO',
        postalCode: '000000',
        phone: '1234567'
      }
    },
    creditCard: {
      number: '', // '4097440000000004'
      securityCode: '', // '321'
      expirationDate: '', // '2018/12'
      name: '' // 'REJECTED'
    },
    extraParameters: {
      INSTALLMENTS_NUMBER: 1
    },
    type: 'AUTHORIZATION_AND_CAPTURE',
    paymentMethod: 'VISA',
    paymentCountry: 'CO',
    deviceSessionId: 'vghs6tvkcle931686k1900o6e1',
    ipAddress: '127.0.0.1',
    cookie: 'pt1t38347bs6jc9ruv2ecpv7o2',
    userAgent: 'Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0'
  };


  constructor(private dataService: BuyTicketDataService,
              private eventsService: EventsService,
              private authenticationService: AuthenticationService,
              private ticketsService: TicketsService,
              private modalService: NgbModal,
              private cookieService: CookieService,
              private sanitizer: DomSanitizer,
              private router: Router
  ) {
    this.step = 'init';
  }

  ngOnInit() {
    this.event = this.dataService.getEvent();
    if (this.event === undefined || this.event === null) {
      this.router.navigate(['/events/list']);
    }

    this.client = this.authenticationService.getCurrentUser() as Client;
    this.requestEventSeats();
  }

  requestEventSeats() {
    this.eventsService.getEventSeats(this.event.id).subscribe(zones => {
      this.eventZones = zones;
      for (const zone of zones) {
        const processedSeats = [];
        for (const seat of zone.event_seat) {
          if (!seat.status) {
            processedSeats.push(seat);
          }
        }
        zone.event_seat = processedSeats;
      }
      this.ticketAmountPerZone = Array(zones.length).fill(0);
    });
  }

  updateTotal() {
    this.totalPrice = 0;
    this.ticketCount = 0;
    for (let index = 0; index < this.eventZones.length; index++) {
      this.ticketCount += this.ticketAmountPerZone[index];
      this.totalPrice += this.ticketAmountPerZone[index] * this.eventZones[index].price;
    }
  }


  goToTicketDetail() {
    this.zoneTickets = [];
    this.zoneSeats = [];
    for (let zoneIndex = 0; zoneIndex < this.eventZones.length; zoneIndex++) {
      const zone = this.eventZones[zoneIndex];
      const tickets: BuyTicket[] = [];
      const seats: EventSeat[] = [];
      for (let seatIndex = 0; seatIndex < this.ticketAmountPerZone[zoneIndex]; seatIndex++ ) {
        const seat = zone.event_seat[seatIndex];
        if (!seat.status) {
          seats.push(seat);
          tickets.push(new BuyTicket('', seat.event_seat_id));
        }
      }
      this.zoneTickets.push(tickets);
      this.zoneSeats.push(seats);
    }
    this.step = 'ticketDetail';
  }

  goToPaymentInfo() {
    const tickets: BuyTicket[] = [];
    for (const zone of this.zoneTickets) {
      if (zone.length) {
        for (const ticket of zone) {
          tickets.push(ticket);
        }
      }
    }

    this.sendTicketReservation(tickets);

    this.step = 'paymentInfo';
  }

  sendTicketReservation(tickets: BuyTicket[]) {
    this.ticketsService.reserveTickets(tickets).subscribe(ticketIds => {
      this.ticketIds = ticketIds;
    });
  }

  sendPayment() {
    this.setTransactionValues();

    this.openModal();

    this.ticketsService.createPayment(this.totalPrice, this.ticketIds, this.transaction).subscribe(response => {
    });

  }

  openModal() {
    this.modalReference = this.modalService.open(this.modalContent, {ariaLabelledBy: 'modal-basic-title'});
    this.modalReference.result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }

  redirectToEventList() {
    this.modalReference.close();
    this.router.navigate(['/tickets/list']);
  }


  setTransactionValues() {
    this.transaction.order.description = `EasyTicket - ${this.event.name} - Payment Test`;
    this.transaction.order.buyer.fullName = `${this.client.first_name} ${this.client.last_name}`;
    this.transaction.order.buyer.emailAddress = `${this.client.email}`;
    this.transaction.order.buyer.dniNumber = `${this.client.document_number}`;
    this.transaction.order.buyer.shippingAddress.street1 = `${this.address}`;
    this.transaction.order.shippingAddress.street1 = `${this.address}`;

    this.transaction.payer.fullName = `${this.client.first_name} ${this.client.last_name}`;
    this.transaction.payer.emailAddress = `${this.client.email}`;
    this.transaction.payer.dniNumber = `${this.client.document_number}`;
    this.transaction.payer.billingAddress.street1 = `${this.address}`;

    this.transaction.creditCard.number = `${this.cardNumber}`;
    this.transaction.creditCard.securityCode = `${this.cvv}`;
    this.transaction.creditCard.expirationDate = `${this.expirationDate}`;
    this.transaction.creditCard.name = `${this.cardName}`;

    this.setDeviceSessionId();
  }

  setDeviceSessionId() {

    const sessionId = this.cookieService.get('sessionid');
    const timestamp = Date.now();
    this.deviceSessionId = Md5.hashStr(`${sessionId}${timestamp}`);
    this.transaction.deviceSessionId = this.deviceSessionId;

    const url = `https://maf.pagosonline.net/ws/fp/tags.js?id=${this.deviceSessionId}80200`;
    this.payUUrl = this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }

  goToInit() {
    this.step = 'init';
  }
}
