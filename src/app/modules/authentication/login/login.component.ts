import {Component, OnInit, ViewChild} from '@angular/core';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username;
  password;
  error;
  errorMessage;

  @ViewChild('loginForm') loginForm: NgForm;

  constructor(private authenticationService: AuthenticationService, private router: Router) {
    this.error = false;
    this.errorMessage = '';
  }

  ngOnInit() {
  }

  login() {
    this.authenticationService.login(this.username, this.password).subscribe(
      loginResponse => {
        this.authenticationService.getUser(this.username).subscribe(
          user => {
            this.error = false;
            this.errorMessage = '';
            this.router.navigate(['/events/list']);
          },
          error => {

          }
        );
      },
      error => {
        this.errorMessage = 'Usuario o contraseña incorrectos';
        this.error = true;
      }
    );
  }
}
