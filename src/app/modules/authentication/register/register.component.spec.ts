import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {RegisterComponent} from './register.component';
import {HttpClientModule} from '@angular/common/http';
import {FormsModule} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {MockAuthenticationService} from '../../../shared/mocks/mock-authentication.service';

describe('RegisterComponent', () => {
  let component: RegisterComponent;
  let fixture: ComponentFixture<RegisterComponent>;

  const router = {
    // Mock function
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        FormsModule
      ],
      declarations: [
        RegisterComponent
      ],
      providers: [
        { provide: Router, useValue: router},
        { provide: AuthenticationService, useClass: MockAuthenticationService }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid on empty form', () => {
    fixture.detectChanges();

    fixture.whenStable().then( () => {
      expect(component.registerForm.valid).toBeFalsy();
    });
  });

  it('should check firstName filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const firstName = component.registerForm.controls['firstName'];

      expect(firstName.valid).toBeFalsy();

      errors = firstName.errors || {};
      expect(errors['required']).toBeTruthy();

      firstName.setValue('test');
      errors = firstName.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(firstName.valid).toBeTruthy();
    });
  });

  it('should check lastName filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const lastName = component.registerForm.controls['lastName'];

      expect(lastName.valid).toBeFalsy();

      errors = lastName.errors || {};
      expect(errors['required']).toBeTruthy();

      lastName.setValue('test');
      errors = lastName.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(lastName.valid).toBeTruthy();
    });
  });

  it('should check email filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const email = component.registerForm.controls['email'];

      expect(email.valid).toBeFalsy();

      errors = email.errors || {};
      expect(errors['required']).toBeTruthy();

      email.setValue('test');
      errors = email.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(errors['email']).toBeTruthy();

      email.setValue('test@mail.com');
      errors = email.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(errors['email']).toBeFalsy();
      expect(email.valid).toBeTruthy();
    });
  });

  it('should check username filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const username = component.registerForm.controls['username'];

      expect(username.valid).toBeFalsy();

      errors = username.errors || {};
      expect(errors['required']).toBeTruthy();

      username.setValue('test');
      errors = username.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(username.valid).toBeTruthy();
    });
  });

  it('should check documentType filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const documentType = component.registerForm.controls['document_type'];

      expect(documentType.valid).toBeFalsy();

      errors = documentType.errors || {};
      expect(errors['required']).toBeTruthy();

      documentType.setValue('');
      errors = documentType.errors || {};
      expect(errors['required']).toBeTruthy();
      expect(documentType.valid).toBeFalsy();

      documentType.setValue('CC');
      errors = documentType.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(documentType.valid).toBeTruthy();
    });
  });

  it('should check documentNumber filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const documentNumber = component.registerForm.controls['document_number'];

      expect(documentNumber.valid).toBeFalsy();

      errors = documentNumber.errors || {};
      expect(errors['required']).toBeTruthy();

      documentNumber.setValue('12345678');
      errors = documentNumber.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(documentNumber.valid).toBeTruthy();
    });
  });

  it('should check birthDate filed validity', () => {
    fixture.whenStable().then( () => {
      let errors: {};
      const birthDate = component.registerForm.controls['birthdate'];

      expect(birthDate.valid).toBeFalsy();

      errors = birthDate.errors || {};
      expect(errors['required']).toBeTruthy();

      birthDate.setValue('01-01-2000');
      errors = birthDate.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(errors['pattern']).toBeTruthy();

      birthDate.setValue('1/1/2000');
      errors = birthDate.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(errors['pattern']).toBeFalsy();

      birthDate.setValue('01/01/2000');
      errors = birthDate.errors || {};
      expect(errors['required']).toBeFalsy();
      expect(errors['pattern']).toBeFalsy();
      expect(birthDate.valid).toBeTruthy();
    });
  });

  it('should navigate on successful registration', () => {
    fixture.whenStable().then(() => {
      const firstName = component.registerForm.controls['firstName'];
      const lastName = component.registerForm.controls['lastName'];
      const email = component.registerForm.controls['email'];
      const username = component.registerForm.controls['username'];
      const password = component.registerForm.controls['password'];
      const documentType = component.registerForm.controls['document_type'];
      const documentNumber = component.registerForm.controls['document_number'];
      const birthDate = component.registerForm.controls['birthdate'];

      firstName.setValue('name');
      lastName.setValue('lastName');
      email.setValue('email@mail.com');
      username.setValue('username');
      password.setValue('password');
      documentType.setValue('CC');
      documentNumber.setValue('12345678');
      birthDate.setValue('01/01/2000');

      expect(firstName.valid).toBeTruthy();
      expect(lastName.valid).toBeTruthy();
      expect(email.valid).toBeTruthy();
      expect(username.valid).toBeTruthy();
      expect(password.valid).toBeTruthy();
      expect(documentType.valid).toBeTruthy();
      expect(documentNumber.valid).toBeTruthy();
      expect(birthDate.valid).toBeTruthy();

      component.register();

      expect(router.navigate).toHaveBeenCalledWith(['/events/list']);
    });
  });

});
