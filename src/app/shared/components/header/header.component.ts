import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {User} from '../../models/user';
import {Router} from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  user: User;

  constructor(private authenticationService: AuthenticationService, private router: Router) {
    this.user = this.authenticationService.getCurrentUser();
    this.authenticationService.userObservable.subscribe(user => {
      this.user = user;
    });
  }

  ngOnInit() { }

  redirectToLogin() {
    this.router.navigate(['/authentication/login']);
  }

  logout() {
    this.authenticationService.logout();
    this.router.navigate(['/events/list']);
  }

  userIsLogged() {
    return !this.authenticationService.userIsGuest();
  }

}
