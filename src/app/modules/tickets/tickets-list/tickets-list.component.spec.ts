import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TicketsListComponent} from './tickets-list.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {Client} from '../../../shared/models/client';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {MockAuthenticationService} from '../../../shared/mocks/mock-authentication.service';
import {TicketsService} from '../../../shared/services/tickets/tickets.service';
import {MockTicketsService} from '../../../shared/mocks/mock-tickets.service';

describe('TicketsListComponent', () => {
  let component: TicketsListComponent;
  let fixture: ComponentFixture<TicketsListComponent>;

  const dummyClient = new Client(
    'client@mail.com',
    'client',
    'first_name',
    'last_name',
    'password',
    '1/1/2000',
    'CC',
    '12456789'
  );

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule, RouterTestingModule],
      declarations: [ TicketsListComponent ],
      providers: [
        { provide: AuthenticationService, useClass: MockAuthenticationService },
        { provide: TicketsService, useClass: MockTicketsService },
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    localStorage.setItem('currentUser', JSON.stringify(dummyClient));
    fixture = TestBed.createComponent(TicketsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
