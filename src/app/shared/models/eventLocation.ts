export class EventLocation {

  id: number;
  address: string;
  name: string;

  constructor(id?: number, address?: string, name?: string) {
    this.id = id;
    this.address = address;
    this.name = name;
  }
}
