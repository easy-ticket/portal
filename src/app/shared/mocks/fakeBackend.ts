// export class FakeBackendInterceptor implements HttpInterceptor {
export class FakeBackendInterceptor {

  constructor() { }

  /*
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    // array in local storage for registered users
    const users = USERS;
    const events = EVENTS;


    // wrap in delayed observable to simulate server api call
    return of(null).pipe(mergeMap(() => {

      // authenticate
      if (request.url.endsWith('/authenticate') && request.method === 'POST') {
        // find if any user matches login credentials
        const filteredUsers = USERS.filter(user => {
          return user.email === request.body.email && user.password === request.body.password;
        });

        if (filteredUsers.length) {
          // if login details are valid return 200 OK with user details and fake jwt token
          const user = filteredUsers[0];
          const body = {
            id: user.id,
            email: user.email,
            name: user.name,
            role: user.role,
          };

          return of(new HttpResponse({ status: 200, body: body }));
        } else {
          // else return 400 bad request
          return throwError({ error: { message: 'Username or password is incorrect' } });
        }
      }


      // Get all events
      if (request.url.endsWith('/events') && request.method === 'GET') {
        return of(new HttpResponse({ status: 200, body: EVENTS }));
      }

      // Get events by organizer
      if (request.url.match(/\/organizer\/\d+$/) && request.method === 'GET') {
        const urlParts = request.url.split('/');
        const id = parseInt(urlParts[urlParts.length - 1], 10);
        const filteredEvents = EVENTS.filter(event => {
          return event.organizerId === id;
        });
        return of(new HttpResponse({ status: 200, body: filteredEvents }));
      }

      // Get event by id
      if (request.url.match(/\/events\/\d+$/) && request.method === 'GET') {
        const urlParts = request.url.split('/');
        const id = parseInt(urlParts[urlParts.length - 1], 10);
        const filteredEvents = EVENTS.filter(event => {
          return event.id === id;
        });
        return of(new HttpResponse({ status: 200, body: filteredEvents[0] }));
      }

      // Edit event
      if (request.url.match(/\/edit\/\d+$/) && request.method === 'PUT') {
      //if (request.url.endsWith('/events/') && request.method === 'PUT') {
        const newEvent = request.body;
        const urlParts = request.url.split('/');
        const id = parseInt(urlParts[urlParts.length - 1], 10);
        const filteredEvents = EVENTS.filter(event => {
          return event.id === id;
        });
        for (let i = 0; i < EVENTS.length; i++) {
          if (EVENTS[i].id === id) {
            EVENTS[i] = newEvent;
            break;
          }
        }
        return of(new HttpResponse({ status: 200, body: newEvent }));
      }

      // Create event
      if (request.url.endsWith('/events/') && request.method === 'POST') {
        // get new event object from post body
        let newEvent = request.body;
        // save new user
        newEvent.id = events.length + 1;
        events.push(newEvent);

        // respond 200 OK
        return of(new HttpResponse({ status: 200 }));
      }

      // Delete event
      if (request.url.match(/\/events\/\d+$/) && request.method === 'DELETE') {
          // find user by id in users array
          let urlParts = request.url.split('/');
          let id = parseInt(urlParts[urlParts.length - 1]);
          for (let i = 0; i < events.length; i++) {
            let event = events[i];
            if (event.id === id) {
              // delete event
              events.splice(i, 1);
              break;
            }
          }
          // respond 200 OK
          return of(new HttpResponse({ status: 200 }));
      }














      // Get all users
      if (request.url.endsWith('/users') && request.method === 'GET') {
        // check for fake auth token in header and return users if valid, this security is implemented server side in a real application
        if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
          return of(new HttpResponse({ status: 200, body: users }));
        } else {
          // return 401 not authorised if token is null or invalid
          return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }
      }

      // get user by id
      if (request.url.match(/\/users\/\d+$/) && request.method === 'GET') {
        // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
        if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
          // find user by id in users array
          let urlParts = request.url.split('/');
          let id = parseInt(urlParts[urlParts.length - 1]);
          let matchedUsers = users.filter(user => { return user.id === id; });
          let user = matchedUsers.length ? matchedUsers[0] : null;

          return of(new HttpResponse({ status: 200, body: user }));
        } else {
          // return 401 not authorised if token is null or invalid
          return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }
      }

      // register user
      if (request.url.endsWith('/users/register') && request.method === 'POST') {
        // get new user object from post body
        let newUser = request.body;

        // validation
        let duplicateUser = users.filter(user => { return user.email === newUser.email; }).length;
        if (duplicateUser) {
          return throwError({ error: { message: 'Username "' + newUser.email + '" is already taken' } });
        }

        // save new user
        newUser.id = users.length + 1;
        users.push(newUser);
        localStorage.setItem('users', JSON.stringify(users));

        // respond 200 OK
        return of(new HttpResponse({ status: 200 }));
      }

      // delete user
      if (request.url.match(/\/users\/\d+$/) && request.method === 'DELETE') {
        // check for fake auth token in header and return user if valid, this security is implemented server side in a real application
        if (request.headers.get('Authorization') === 'Bearer fake-jwt-token') {
          // find user by id in users array
          let urlParts = request.url.split('/');
          let id = parseInt(urlParts[urlParts.length - 1]);
          for (let i = 0; i < users.length; i++) {
            let user = users[i];
            if (user.id === id) {
              // delete user
              users.splice(i, 1);
              localStorage.setItem('users', JSON.stringify(users));
              break;
            }
          }

          // respond 200 OK
          return of(new HttpResponse({ status: 200 }));
        } else {
          // return 401 not authorised if token is null or invalid
          return throwError({ status: 401, error: { message: 'Unauthorised' } });
        }
      }

      // pass through any requests not handled above
      return next.handle(request);

    }))

    // call materialize and dematerialize to ensure delay even if an error is thrown(https://github.com/Reactive-Extensions/RxJS/issues/648)
      .pipe(materialize())
      .pipe(delay(500))
      .pipe(dematerialize());
  }
}

export let fakeBackendProvider = {
  provide: HTTP_INTERCEPTORS,
  useClass: FakeBackendInterceptor,
  multi: true
  */
}
