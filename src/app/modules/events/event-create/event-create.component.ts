import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {FormArray, FormBuilder, FormGroup, Validators} from '@angular/forms';
import {EventsService} from '../../../shared/services/events/events.service';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {Event} from '../../../shared/models/event';
import {NgbDateStruct, NgbTimeStruct} from '@ng-bootstrap/ng-bootstrap';
import {EventType} from '../../../shared/models/eventType';
import {EventLocation} from '../../../shared/models/eventLocation';
import {Locality} from '../../../shared/models/locality';
import {LocationsService} from '../../../shared/services/locations/locations.service';

@Component({
  selector: 'app-event-create',
  templateUrl: './event-create.component.html',
  styleUrls: ['./event-create.component.css']
})
export class EventCreateComponent implements OnInit {

  event: Event = new Event();
  eventDate: NgbDateStruct;
  eventTime: NgbTimeStruct;
  meridian = true;

  zonesForm: FormGroup;
  zones: FormArray;
  totalSeats = 0;
  focusedSeatNumber = 0;

  event_types: EventType[];
  locations: EventLocation[];

  constructor(
    private router: Router,
    private eventsService: EventsService,
    private locationsService: LocationsService,
    private authenticationService: AuthenticationService,
    private formBuilder: FormBuilder
  ) { }

  ngOnInit() {
    this.zones = this.formBuilder.array([this.createZone()]);
    this.zonesForm = this.formBuilder.group({
      zones: this.zones
    });

    this.requestEventTypes();
    this.requestLocations();
  }

  requestEventTypes() {
    this.eventsService.getEventTypes().subscribe(
      eventTypes => {
        this.event_types = eventTypes;
      }
    );
  }

  requestLocations() {
    this.locationsService.getLocations().subscribe(
      locations => {
        this.locations = locations;
      }
    );
  }

  createZone(): FormGroup {
    return this.formBuilder.group({
      name: ['', Validators.required],
      seats: ['', Validators.required],
      price: ['', Validators.required]
    });
  }

  addZone() {
    this.zones = this.zonesForm.get('zones') as FormArray;
    this.zones.push(this.createZone());
  }

  removeZone(index: number) {
    this.totalSeats -= this.zones.at(index).get('seats').value;
    this.zones.removeAt(index);
  }

  updateFocusedSeatNumber(index: number) {
    this.focusedSeatNumber = this.zones.at(index).get('seats').value;
  }

  updateTotalSeats(index: number) {
    this.totalSeats -= this.focusedSeatNumber;
    this.totalSeats += this.zones.at(index).get('seats').value;
  }

  createEvent() {
    const zonesList: Locality[] = [];
    for (const zone of this.zones.controls) {
      const zoneName = zone.get('name').value;
      const zoneCapacity = zone.get('seats').value;
      const zonePrice = zone.get('price').value;
      zonesList.push(new Locality(zoneName, zoneCapacity, zonePrice));
    }

    this.event.date = `${this.eventDate.day}/${this.eventDate.month}/${this.eventDate.year}`;
    this.event.date += ` ${this.eventTime.hour}:${this.eventTime.minute}:${this.eventTime.second}`;

    this.eventsService.createEvent(this.event, zonesList).subscribe(
    response => {
      this.router.navigate(['/events/list']);
      }, error => {

      }
    );

  }

}
