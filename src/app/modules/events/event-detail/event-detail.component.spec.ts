import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventDetailComponent} from './event-detail.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HttpClientModule} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {EventsService} from '../../../shared/services/events/events.service';
import {MockEventsService} from '../../../shared/mocks/mock-events.service';
import {LocationsService} from '../../../shared/services/locations/locations.service';
import {MockLocationsService} from '../../../shared/mocks/mock-locations.service';
import {ReadableDatePipe} from '../../../shared/pipes/readable-date.pipe';
import {RouterTestingModule} from '@angular/router/testing';

describe('EventDetailComponent', () => {
  let component: EventDetailComponent;
  let fixture: ComponentFixture<EventDetailComponent>;

  const router = {
    // Mock function
    navigate: jasmine.createSpy('navigate')
  };
  const route = {
    snapshot: {
      paramMap: jasmine.createSpyObj('ParamsMap', {
        'get': 1
      })
    }
  };


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        HttpClientModule,
        RouterTestingModule
      ],
      declarations: [
        EventDetailComponent,
        ReadableDatePipe
      ],
      providers: [
        { provide: Router, useValue: router },
        { provide: ActivatedRoute, useValue: route },
        { provide: EventsService, useClass: MockEventsService },
        { provide: LocationsService, useClass: MockLocationsService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
