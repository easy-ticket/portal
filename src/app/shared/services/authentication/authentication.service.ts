import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {User} from '../../models/user';
import {Observable, Subject, throwError} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {Client} from '../../models/client';
import {Token} from '../../models/token';
import {Organizer} from '../../models/organizer';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {

  private readonly apiUrl: string;
  private user: User;
  private token: string;
  private role: string;

  public userObservable = new Subject<User>();

  constructor(private http: HttpClient) {
    this.user = JSON.parse(localStorage.getItem('currentUser'));
    this.token = localStorage.getItem('currentToken');
    this.role = localStorage.getItem('currentRole');
    this.apiUrl = environment.apiUrl;
    this.notifyUserChange(this.user);
  }

  private getHttpHeaders() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Token: ${this.token}`
      })
    };
  }

  userIsOrganizer() {
    return this.role === 'organizer';
  }

  userIsClient() {
    return this.role === 'client';
  }

  userIsGuest() {
    return this.user === null || this.user === undefined;
  }

  notifyUserChange(user) {
    this.userObservable.next(user);
  }

  getCurrentUser() {
    return this.user;
  }

  getCurrentToken() {
    return this.token;
  }

  getCurrentRole() {
    return this.role;
  }

  private setCurrentUser(user: User) {
    this.user = user;
    if (this.user !== null) {
      localStorage.setItem('currentUser', JSON.stringify(this.user));
    }
    this.notifyUserChange(this.user);
  }

  private setCurrentToken(token: string) {
    this.token = token;
    if (this.token !== '') {
      localStorage.setItem('currentToken', this.token);
    }
  }

  private setCurrentRole(role: string) {
    this.role = role;
    if (this.role !== '') {
      localStorage.setItem('currentRole', this.role);
    }
  }

  login(username: string, password: string): Observable<Token> {
    const url = `${this.apiUrl}/token/`;
    this.logout();
    const loginData = {
      username: username,
      password: password
    };
    return this.http.post<Token>(url, loginData)
      .pipe(map(token => {
        if (token) {
          this.setCurrentToken(token.token);
          this.setCurrentRole(token.role);
        }
        return token;
      }), catchError((error: HttpErrorResponse) => {
        console.log(error);
        return throwError(new Error(''));
      }));
  }

  getUser(username: string): Observable<object> {
    if (this.role === 'organizer') {
      return this.getOrganizer(username);
    } else if (this.role === 'client') {
      return this.getClient(username);
    }
  }

  private getClient(username: string): Observable<object> {
    const url = `${this.apiUrl}/user/client/${username}/`;
    return this.http.get<object>(url, this.getHttpHeaders())
      .pipe(map(client => {
        const user = new Client(
          client['user']['email'],
          client['user']['username'],
          client['user']['first_name'],
          client['user']['last_name'],
          client['user']['password'],
          client['birthdate'],
          client['document_number'],
          client['document_type']
        );
        this.setCurrentUser(user);
        return client;
      }));
  }

  private getOrganizer(username: string): Observable<object> {
    const url = `${this.apiUrl}/user/organizer/${username}/`;
    return this.http.get<object>(url, this.getHttpHeaders())
      .pipe(map(organizer => {
        const user = new Organizer(
          organizer['user']['email'],
          organizer['user']['username'],
          organizer['user']['first_name'],
          organizer['user']['last_name'],
          organizer['user']['password'],
          organizer['contact_email'],
          organizer['contact_name'],
          organizer['nit'],
          organizer['phone'],
        );
        this.setCurrentUser(user);
        return organizer;
      }));
  }

  register(client: Client): Observable<object> {
    const url = `${this.apiUrl}/user/clients/`;
    const clientData = {
      user: {
        email: client.email,
        username: client.username,
        first_name: client.first_name,
        last_name: client.last_name,
        password: client.password,
      },
      birthdate: client.birthdate,
      document_number: client.document_number,
      document_type: client.document_type,
    };
    return this.http.post<object>(url, clientData);
  }

  registerOrganizer(organizer: Organizer): Observable<object> {
    const url = `${this.apiUrl}/user/organizers/`;
    const organizerData = {
      user: {
        email: organizer.email,
        username: organizer.username,
        first_name: organizer.first_name,
        last_name: organizer.last_name,
        password: organizer.password,
      },
      contact_email: organizer.contact_email,
      contact_name: organizer.contact_name,
      name: organizer.name,
      nit: organizer.nit,
      phone: organizer.phone,
    };
    return this.http.post<object>(url, organizerData);
  }

  logout() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentToken');
    localStorage.removeItem('currentRole');
    this.setCurrentUser(null);
    this.setCurrentToken('');
    this.setCurrentRole('');
  }
}
