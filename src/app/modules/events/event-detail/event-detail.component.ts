import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {EventsService} from '../../../shared/services/events/events.service';
import {Event} from '../../../shared/models/event';
import {EventType} from '../../../shared/models/eventType';
import {EventLocation} from '../../../shared/models/eventLocation';
import {LocationsService} from '../../../shared/services/locations/locations.service';
import {NgbCarouselConfig} from '@ng-bootstrap/ng-bootstrap';
import {BuyTicketDataService} from '../../../shared/services/intermediateServices/buy-ticket-data.service';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';

@Component({
  selector: 'app-event-detail',
  templateUrl: './event-detail.component.html',
  styleUrls: ['./event-detail.component.css'],
  providers: [NgbCarouselConfig]
})
export class EventDetailComponent implements OnInit {

  event: Event;
  eventType: EventType;
  location: EventLocation;

  images;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private eventsService: EventsService,
              private authenticationService: AuthenticationService,
              private locationsService: LocationsService,
              private buyTicketDataService: BuyTicketDataService,
              private carouselConfig: NgbCarouselConfig
  ) {
    this.images = [1, 2, 3].map(() => `https://picsum.photos/300/450?random&t=${Math.random()}`);
    carouselConfig.interval = 7000;
  }

  ngOnInit() {
    this.requestEvent();
  }

  requestEvent() {
    const id = +this.route.snapshot.paramMap.get('id');
    this.eventsService.getEventById(id).subscribe(event => {
      this.event = event;
      this.requestEventType();
      this.requestLocation();
    });
  }

  requestEventType() {
    this.eventsService.getEventTypeById(this.event.event_type).subscribe(type => {
      this.eventType = type;
    });
  }

  requestLocation() {
    this.locationsService.getLocationById(this.event.location).subscribe(location => {
      this.location = location;
    });
  }

  redirectToBuyTicket () {
    this.buyTicketDataService.setEvent(this.event);
    this.router.navigate(['tickets/buy']);
  }

  isClient() {
    return this.authenticationService.userIsClient();
  }
}
