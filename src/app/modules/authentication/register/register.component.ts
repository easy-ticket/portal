import {Component, OnInit, ViewChild} from '@angular/core';
import {Client} from '../../../shared/models/client';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {Router} from '@angular/router';
import {NgForm} from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @ViewChild('registerForm') registerForm: NgForm;

  client: Client = new Client('', '', '', '', '', '', '', '');
  document_types = [
    {code: '', value: 'Tipo'},
    {code: 'CC', value: 'Cédula de ciudadanía'},
    {code: 'CE', value: 'Cédula de extranjería'},
    {code: 'TI', value: 'Tarjeta de identidad'},
  ];

  constructor(private authenticationService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {

  }

  register() {

    this.authenticationService.register(this.client).subscribe(
      registerResponse => {
        this.authenticationService.login(this.client.username, this.client.password).subscribe(
          loginResponse => {
            this.authenticationService.getUser(this.client.username).subscribe(
              user => {
                this.router.navigate(['/events/list']);
              },
              error => {}
            );
          },
          error => {}
        );
      },
      error => {}
    );
  }

}
