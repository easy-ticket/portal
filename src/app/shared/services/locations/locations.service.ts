import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthenticationService} from '../authentication/authentication.service';
import {environment} from '../../../../environments/environment';
import {Observable} from 'rxjs';
import {EventLocation} from '../../models/eventLocation';

@Injectable({
  providedIn: 'root'
})
export class LocationsService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    this.apiUrl = environment.apiUrl;
  }

  getHttpHeaders() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Token: ${this.authenticationService.getCurrentToken()}`
      })
    };
  }

  getLocations(): Observable<EventLocation[]> {
    const url = `${this.apiUrl}/location/`;
    return this.http.get<EventLocation[]>(url, this.getHttpHeaders());
  }

  getLocationById(id: number): Observable<EventLocation> {
    const url = `${this.apiUrl}/location/${id}/`;
    return this.http.get<EventLocation>(url, this.getHttpHeaders());
  }
}
