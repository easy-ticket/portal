export class TicketData {
  id: number;
  ticket_number: number;
  document_number: string;
  client: number;
  event_seat: number;
  status: number;
  event_id: number;
  seat_number: string;
  event_name: string;

  constructor (id?: number, ticket_number?: number, document_number?: string,
               client?: number, event_seat?: number, status?: number, event_id?: number,
               seat_number?: string, event_name?: string) {
    this.id = id;
    this.ticket_number = ticket_number;
    this.document_number = document_number;
    this.client = client;
    this.event_seat = event_seat;
    this.status = status;
    this.event_id = event_id;
    this.seat_number = seat_number;
    this.event_name = event_name;
  }
}
