import {TestBed} from '@angular/core/testing';

import {MockLocationsService} from './mock-locations.service';

describe('MockLocationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockLocationsService = TestBed.get(MockLocationsService);
    expect(service).toBeTruthy();
  });
});
