import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {FormsModule} from '@angular/forms';
import {AuthenticationRoutingModule} from './authentication-routing.module';
import {RegisterOrganizerComponent} from './register-organizer/register-organizer.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    AuthenticationRoutingModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    RegisterOrganizerComponent
  ]
})
export class AuthenticationModule {
}
