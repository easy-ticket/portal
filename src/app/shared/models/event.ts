export class Event {
  id: number;
  date: string;
  description: string;
  duration: number;
  name: string;
  status: number;
  event_type: number;
  location: number;

  constructor(id?: number, date?: string, description?: string, duration?: number, name?: string, status?: number, event_type?: number,
              location?: number) {
    this.id = id;
    this.date = date;
    this.description = description;
    this.duration = duration;
    this.name = name;
    this.status = status;
    this.event_type = event_type;
    this.location = location;
  }
}
