import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../../../shared/services/authentication/authentication.service';
import {Router} from '@angular/router';
import {Organizer} from '../../../shared/models/organizer';

@Component({
  selector: 'app-register-organizer',
  templateUrl: './register-organizer.component.html',
  styleUrls: ['./register-organizer.component.css']
})
export class RegisterOrganizerComponent implements OnInit {

  organizer: Organizer = new Organizer();

  constructor(private authenticationService: AuthenticationService, private router: Router) {
  }

  ngOnInit() {

  }

  registerOrganizer() {
    this.organizer.email = this.organizer.contact_email;
    this.organizer.first_name = this.organizer.name;
    this.authenticationService.registerOrganizer(this.organizer).subscribe(
      registerResponse => {
        this.authenticationService.login(this.organizer.username, this.organizer.password).subscribe(
          loginResponse => {
            this.authenticationService.getUser(this.organizer.username).subscribe(
              user => {
                this.router.navigate(['/events/list']);
              },
              error => {}
            );
          },
          error => {}
        );
      },
      error => {}
    );
  }

}
