import {TestBed} from '@angular/core/testing';

import {TicketsService} from './tickets.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('TicketsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
  }));

  it('should be created', () => {
    const service: TicketsService = TestBed.get(TicketsService);
    expect(service).toBeTruthy();
  });
});
