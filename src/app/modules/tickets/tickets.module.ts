import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {TicketsRoutingModule} from './tickets-routing.module';
import {TicketBuyComponent} from './ticket-buy/ticket-buy.component';
import {FormsModule} from '@angular/forms';
import {CookieService} from 'ngx-cookie-service';
import {TicketsListComponent} from './tickets-list/tickets-list.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgbModule,
    TicketsRoutingModule
  ],
  declarations: [TicketBuyComponent, TicketsListComponent],
  providers: [CookieService]
})
export class TicketsModule { }
