import {Injectable} from '@angular/core';
import {EventLocation} from '../models/eventLocation';
import {Observable, of} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MockLocationsService {

  locations: EventLocation[] = [
    new EventLocation(1, 'Test address', 'Location 1')
  ];

  constructor() { }

  getLocations(): Observable<EventLocation[]> {
    return of(this.locations);
  }

  getLocationById(id: number): Observable<EventLocation> {
    let specificLocation: EventLocation;
    for (const location of this.locations) {
      if (location.id === id) {
        specificLocation = location;
        break;
      }
    }
    return of(specificLocation);
  }
}
