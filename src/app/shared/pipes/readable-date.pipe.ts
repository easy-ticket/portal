import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'readableDate'
})
export class ReadableDatePipe implements PipeTransform {

  months = [
    'enero', 'febrero', 'marzo', 'abril', 'mayo', 'junio', 'julio', 'agosto', 'septiembre', 'octubre', 'noviembre', 'diciembre'
  ];

  transform(dateString: string): string {
    const dateList = dateString.split(' ');
    const dateArray = dateList[0].split('/').map(num => parseInt(num, 10));
    const timeArray = dateList[1].split(':');

    const day = dateArray[0];
    const month = this.months[dateArray[1] - 1];
    const year = dateArray[2];

    let hour = parseInt(timeArray[0], 10);
    const minutes = timeArray[1];
    let meridian;

    if (hour > 12) {
      hour -= 12;
      meridian = 'p.m.';
    } else {
      meridian = 'a.m.';
      if (parseInt(minutes, 10) > 0) {
        meridian = 'p.m.';
      }
    }

    return `${day} de ${month} de ${year}, ${hour}:${minutes} ${meridian}`;
  }

}
