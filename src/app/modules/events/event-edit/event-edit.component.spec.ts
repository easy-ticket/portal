import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {EventEditComponent} from './event-edit.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {ActivatedRoute, Router} from '@angular/router';
import {EventsService} from '../../../shared/services/events/events.service';
import {MockEventsService} from '../../../shared/mocks/mock-events.service';
import {LocationsService} from '../../../shared/services/locations/locations.service';
import {MockLocationsService} from '../../../shared/mocks/mock-locations.service';

describe('EventEditComponent', () => {
  let component: EventEditComponent;
  let fixture: ComponentFixture<EventEditComponent>;

  const router = {
    // Mock function
    navigate: jasmine.createSpy('navigate')
  };
  const route = {
    snapshot: {
      paramMap: {
        get: jasmine.createSpy('get')
      }
    }
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        NgbModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule
      ],
      declarations: [
        EventEditComponent
      ],
      providers: [
        { provide: Router, useValue: router },
        { provide: ActivatedRoute, useValue: route },
        { provide: EventsService, useClass: MockEventsService },
        { provide: LocationsService, useClass: MockLocationsService }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    // Mock value for event id
    route.snapshot.paramMap.get.and.returnValue(1);

    fixture = TestBed.createComponent(EventEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
