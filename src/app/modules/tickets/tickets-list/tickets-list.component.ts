import {Component, OnInit} from '@angular/core';
import {TicketData} from '../../../shared/models/ticket-data';
import {TicketsService} from '../../../shared/services/tickets/tickets.service';
import {ModalDismissReasons, NgbModal} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-tickets-list',
  templateUrl: './tickets-list.component.html',
  styleUrls: ['./tickets-list.component.css']
})
export class TicketsListComponent implements OnInit {

  tickets: TicketData[];

  firstColumn: TicketData[];
  secondColumn: TicketData[];

  closeResult: string;

  constructor(private ticketsService: TicketsService, private modalService: NgbModal) { }

  ngOnInit() {
    this.requestTickets();
  }

  requestTickets() {
    this.ticketsService.getTicketsByUsername().subscribe(
      tickets => {
        this.tickets = tickets;
        const half = Math.floor(tickets.length / 2);
        this.firstColumn = this.tickets.slice(0, half);
        this.secondColumn = this.tickets.slice(half, this.tickets.length);
      }
    );
  }

  requestTicketQrCode(id: number) {
    this.ticketsService.requestTicketQRCodeById(id).subscribe(
      response => {

    });
  }

  openModal(content, ticket) {
    this.requestTicketQrCode(ticket.id);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return  `with: ${reason}`;
    }
  }
}
