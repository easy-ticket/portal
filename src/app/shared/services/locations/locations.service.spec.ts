import {TestBed} from '@angular/core/testing';

import {LocationsService} from './locations.service';
import {HttpClientTestingModule} from '@angular/common/http/testing';

describe('LocationsService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ]
  }));

  it('should be created', () => {
    const service: LocationsService = TestBed.get(LocationsService);
    expect(service).toBeTruthy();
  });
});
