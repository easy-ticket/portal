import {TestBed} from '@angular/core/testing';

import {MockAuthenticationService} from './mock-authentication.service';

describe('MockAuthenticationService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MockAuthenticationService = TestBed.get(MockAuthenticationService);
    expect(service).toBeTruthy();
  });
});
