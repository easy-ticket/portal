import {async, ComponentFixture, TestBed} from '@angular/core/testing';

import {TicketBuyComponent} from './ticket-buy.component';
import {FormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {CookieService} from 'ngx-cookie-service';
import {RouterTestingModule} from '@angular/router/testing';
import {BuyTicketDataService} from '../../../shared/services/intermediateServices/buy-ticket-data.service';
import {MockBuyTicketDataService} from '../../../shared/mocks/mock-buy-ticket-data.service';

describe('TicketBuyComponent', () => {
  let component: TicketBuyComponent;
  let fixture: ComponentFixture<TicketBuyComponent>;


  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [FormsModule, HttpClientModule, RouterTestingModule],
      declarations: [ TicketBuyComponent ],
      providers: [
        CookieService,
        {provide: BuyTicketDataService, useClass: MockBuyTicketDataService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TicketBuyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
