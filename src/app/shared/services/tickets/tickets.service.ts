import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthenticationService} from '../authentication/authentication.service';
import {environment} from '../../../../environments/environment';
import {BuyTicket} from '../../models/buy-ticket';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {TicketData} from '../../models/ticket-data';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {

  private readonly apiUrl: string;

  constructor(private http: HttpClient, private authenticationService: AuthenticationService) {
    this.apiUrl = environment.apiUrl;
  }

  getHttpHeaders() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': `Token: ${this.authenticationService.getCurrentToken()}`
      })
    };
  }

  reserveTickets(tickets: BuyTicket[]) {
    const client = this.authenticationService.getCurrentUser();
    const url = `${this.apiUrl}/user/client/${client.username}/tickets/`;
    return this.http.post(url, tickets, this.getHttpHeaders()).pipe(
      map(response => {
        const ids = [];
        for (const id of response['tickets_id']) {
          ids.push(id);
        }
        return ids;
      })
    );
  }

  createPayment(totalPrice: number, ticketIds: number[], transactionInformation: any) {
    const client = this.authenticationService.getCurrentUser();
    const url = `${this.apiUrl}/user/client/${client.username}/payments/`;

    const data = {
      total: totalPrice,
      ticket: ticketIds,
      payment_data: {
        transaction: transactionInformation
      }
    };

    return this.http.post(url, data, this.getHttpHeaders());
  }

  getTicketsByUsername(): Observable<TicketData[]> {
    const client = this.authenticationService.getCurrentUser();
    const url = `${this.apiUrl}/user/client/${client.username}/tickets/`;

    return this.http.get<TicketData[]>(url, this.getHttpHeaders());
  }

  requestTicketQRCodeById(id: number) {
    const client = this.authenticationService.getCurrentUser();
    const url = `${this.apiUrl}/user/client/${client.username}/tickets/${id}/qr_code/`;

    return this.http.get(url, this.getHttpHeaders());
  }
}
