import {async, ComponentFixture, TestBed} from '@angular/core/testing';
import {HeaderComponent} from './header.component';
import {HttpClientModule} from '@angular/common/http';
import {RouterTestingModule} from '@angular/router/testing';
import {AuthenticationService} from '../../services/authentication/authentication.service';
import {MockAuthenticationService} from '../../mocks/mock-authentication.service';
import {Client} from '../../models/client';

describe('HeaderComponent', () => {
  let component: HeaderComponent;
  let fixture: ComponentFixture<HeaderComponent>;

  const dummyClient = new Client(
    'client@mail.com',
    'client',
    'first_name',
    'last_name',
    'password',
    '1/1/2000',
    'CC',
    '12456789'
  );

  beforeEach(() => {
    localStorage.removeItem('currentUser');
  });

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        RouterTestingModule,
      ],
      declarations: [
        HeaderComponent
      ],
      providers: [
        {provide: AuthenticationService, useClass: MockAuthenticationService}
      ]
    })
    .compileComponents();
  }));

  it('should create', () => {
    fixture = TestBed.createComponent(HeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    expect(component).toBeTruthy();
  });
  it('should render title', async(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('h1').textContent).toContain('Easy Ticket');
  }));
  it('should render login button', async(() => {
    fixture = TestBed.createComponent(HeaderComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('button').textContent).toContain('Iniciar sesión');
  }));
  it('should render logout button when user is logged', async(() => {
    localStorage.setItem('currentUser', JSON.stringify(dummyClient));
    fixture = TestBed.createComponent(HeaderComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('button').textContent).toContain('Cerrar sesión');
    localStorage.removeItem('currentUser');
  }));
  it('should render user information when user is logged', async(() => {
    localStorage.setItem('currentUser', JSON.stringify(dummyClient));
    fixture = TestBed.createComponent(HeaderComponent);
    fixture.detectChanges();
    const compiled = fixture.debugElement.nativeElement;
    expect(compiled.querySelector('.user-first-name').textContent).toContain('first_name');
    localStorage.removeItem('currentUser');
  }));
});
